local utility_lib = {}

BLACK_SPOT_COLOR_UNDER_VALUE = 0.2
HEADQUARTER_SPOT_COLOR_UNDER_VALUE = 0.6
HEADQUARTER_SPOT_COLOR_ABOVE_VALUE = 0.35

EXPLORED_AREA_NEAR_RANGE = 0.4

function utility_lib.is_on_headquarter()
    onspot = false
    nground = 0
    for i = 1, #robot.motor_ground do
        if robot.motor_ground[i].value >= HEADQUARTER_SPOT_COLOR_ABOVE_VALUE and
                robot.motor_ground[i].value <= HEADQUARTER_SPOT_COLOR_UNDER_VALUE then
            nground = nground + 1
        end
    end
    if nground == 4 then
        onspot = true
    end
    return onspot
end

function utility_lib.is_already_explored(robot_position, already_explored_areas)
    for i = 1, #already_explored_areas do
        for j = 1, #already_explored_areas[i] do
            if math.abs(robot_position.x - already_explored_areas[i][j].x) < EXPLORED_AREA_NEAR_RANGE and
                math.abs(robot_position.y - already_explored_areas[i][j].y) < EXPLORED_AREA_NEAR_RANGE and
                math.abs(robot_position.z - already_explored_areas[i][j].z) < EXPLORED_AREA_NEAR_RANGE then
                
                return true
            end
        end
    end
    return false
end

function utility_lib.is_on_explorating_area()
    for i = 1, #robot.motor_ground do
        if robot.motor_ground[i].value <= BLACK_SPOT_COLOR_UNDER_VALUE then
            return true
        end
    end
    return false
end

return utility_lib