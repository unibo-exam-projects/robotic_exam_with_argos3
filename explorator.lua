-- Example code of a possible implementation of the subsumption architecure
-- 
-- 1. Inizialmente girare casualmente per l'arena per trovare delle zone colorate da esplorare.
-- 2. Quando le trova salva la loro posizione e ritorna alla base (tramite phototaxis) per depositare i dati.
-- 3. Dopo di che riesce in cerca di una nuova zona da esplorare.
-- 4. Se l'area che incontra è già stata esplorata la ignora e ne cerca un'altra.
-- 5. Una volta che il robot le ha esplorate tutte (sapendo a priori quante sono) torna alla base
--		 e si "mette in carica" (lo simulo facendolo fermare alla base e accendendo un led rosso magari).
--
-- LEVELS for exploring task:
-- 6) OA --> Obstacle Avoidance
-- 5) EW --> check work is end
-- 4) GH --> Get Headquarter
-- 3) OH --> Getout Headquarter
-- 2) EA --> Exploring Area
-- 1) RW --> Random Walk


utility_lib = require "utility_lib"

-- Put your global variables here

MOVE_STEPS = 15
MAX_VELOCITY = 10
VFACTOR = 5
PROX_THRESHOLD = 0.2
LIGHT_THRESHOLD = 0.01 -- it should be set at a value not lower than noise
EXPLORATING_TIME_VALUE = 400
TOTAL_BATTERY_CHARGING_STEPS = 300
TOTAL_BATTERY_POINT = 2
TOTAL_EXPLORATING_AREAS = 6


n_steps = -1
exploring_step = 0
explored_area_flag = false
already_explorated_area_list = {}
tmp_already_explorated_area_list = {}

explored_area_counter = 0
battery_charging_steps = 0
battery_consumed_point_counter = 0

function limit_velocity(v)
	if v > MAX_VELOCITY then
		return MAX_VELOCITY
	elseif v < -MAX_VELOCITY then
		return -MAX_VELOCITY
	else
		return v
	end
end


-- look for new exploring area
function random_walk(lock, velocities)
	mylock = true
	myvelocities = {}
	n_steps = n_steps + 1
	if n_steps % MOVE_STEPS == 0 then
		left_v = robot.random.uniform(0,MAX_VELOCITY)
		right_v = robot.random.uniform(0,MAX_VELOCITY)
	end

	myvelocities = {left = limit_velocity(left_v), right = limit_velocity(right_v)}
	
	if not lock then
		log("RW")
		robot.leds.set_all_colors("black")
		return mylock, myvelocities
	else
		return lock, velocities
	end
end


-- exploring area --> lock when inside a new area
function exploring_area(lock, velocities)
	mylock = false
	myvelocities = {}

	-- check if robot is in a new area
	if utility_lib.is_on_explorating_area() and explored_area_flag == false then
		exploring_step = exploring_step + 1
		robot_position = {x=robot.positioning.position.x, y=robot.positioning.position.y, z=robot.positioning.position.z}
		if utility_lib.is_already_explored(robot_position, already_explorated_area_list) then
			exploring_step = 0
			table.insert(already_explorated_area_list, tmp_already_explorated_area_list)
			tmp_already_explorated_area_list = {}
			log("Detected an explorated area already known")
		else
			table.insert(tmp_already_explorated_area_list, robot_position)
		end
	end

	-- keep explorating until EXPLORATING_TIME_VALUE
	if exploring_step <= EXPLORATING_TIME_VALUE and exploring_step > 0 then
		mylock = true
		log("exploring")
	elseif exploring_step > EXPLORATING_TIME_VALUE then
		explored_area_flag = true
		log("explored")	
	end

	-- random walk keeping robot inside the new area if not ended to exploring it
	left_v = robot.random.uniform(0,MAX_VELOCITY)
	right_v = robot.random.uniform(0,MAX_VELOCITY)

	if robot.motor_ground[1].value > 0.8 then
		myvelocities = {left = MAX_VELOCITY, right = 0}
	elseif robot.motor_ground[4].value > 0.8 then
		myvelocities = {left = 0, right = MAX_VELOCITY}
	else
		myvelocities = {left = left_v, right = right_v}
	end

	-- when ended to exploring it add it in explored list and enable explored_area_flag
	
	
	if not lock and mylock then
		log("EA")
		robot.leds.set_all_colors("green")
		return mylock, myvelocities
	else
		return lock, velocities
	end
end


-- keep outside headquarter when has no data to deliver --> reverse phototaxis
function getout_headquarter(lock, velocities)
	mylock = false
	myvelocities = {}

	if explored_area_flag == false and utility_lib.is_on_headquarter() then
		mylock = true
	end

	-- reverse phototaxis
	value = 0
	idx = -1
	for i = 1,#robot.light do
		if value < robot.light[i].value then
			value = robot.light[i].value
			idx = i
		end
	end

	if value > LIGHT_THRESHOLD then
		if idx == 12 or idx == 13 then
			myvelocities = {left = MAX_VELOCITY , right = MAX_VELOCITY}
		elseif idx > 13 and idx <= 24 then
			myvelocities = {left = 0, right = MAX_VELOCITY}
		elseif idx < 12 and idx >= 1 then
			myvelocities = {left = MAX_VELOCITY, right = 0}
		end
	end
	-- end reverse phototaxis

	if not lock and mylock then
		log("OH")
		robot.leds.set_all_colors("blue")
		return mylock, myvelocities
	else
		return lock, velocities
	end
end


-- get headquarter --> lock when robot end to exploring area
function get_headquarter(lock, velocities)
	mylock = false
	myvelocities = {}

	-- phototaxis
	value = 0
	idx = -1
	for i = 1,#robot.light do
		if value < robot.light[i].value then
			value = robot.light[i].value
			idx = i
		end
	end

	if value > LIGHT_THRESHOLD then
		if idx == 1 or idx == 24 then
			myvelocities = {left = MAX_VELOCITY , right = MAX_VELOCITY}
		elseif idx > 1 and idx <= 12 then
			myvelocities = {left = 0, right = MAX_VELOCITY}
		elseif idx < 24 and idx >= 13 then
			myvelocities = {left = MAX_VELOCITY, right = 0}
		end
		-- check if robot has already explored the area, instead ignore this task
		if explored_area_flag then
			mylock = true
		end
		-- end check
	end
	-- end phototaxis

	-- check if robot get headquarter
	if utility_lib.is_on_headquarter() then
		-- increase number of explorated areas only once, at each area explorated robot uses one battery point
		if explored_area_flag then
			battery_consumed_point_counter = battery_consumed_point_counter + 1
			explored_area_counter = explored_area_counter + 1
		end
		explored_area_flag = false
		exploring_step = 0
		table.insert(already_explorated_area_list, tmp_already_explorated_area_list)
		tmp_already_explorated_area_list = {}
		log("reset")
	end
	-- end check if is on headquarter
	if not lock and mylock then
		log("GH")
		robot.leds.set_all_colors("yellow")
		return mylock, myvelocities
	else
		return lock, velocities
	end
end


-- end work --> check how much area it has checked, once done all, it waits for some steps and then reset all.
function end_work(lock, velocities)
	mylock = false
	myvelocities = {}
	
	-- stops robot when it is on headquarter and it has visited VISITED_AREA_VALUE different areas
	if utility_lib.is_on_headquarter() and battery_consumed_point_counter >= TOTAL_BATTERY_POINT then
		mylock = true

		if explored_area_counter < TOTAL_EXPLORATING_AREAS then
			-- charging ends after CHARGING_STEPS_VALUE steps
			if battery_charging_steps < TOTAL_BATTERY_CHARGING_STEPS then
				battery_charging_steps = battery_charging_steps + 1
			else
				battery_consumed_point_counter = 0
				battery_charging_steps = 0
				log(battery_consumed_point_counter)
				log(LIMITS_NUMBER_VISITED_AREA)
				log("reset visited area")
			end
		else
			log("ENDED all explorable areas")
		end
		myvelocities = {left = 0, right = 0}
	end

	if not lock and mylock then
		log("EW")
		robot.leds.set_all_colors("red")
		return mylock, myvelocities
	else
		return lock, velocities
	end
end


-- avoid collision --> lock when robot meet an obstacle
function collision_avoidance(lock, velocities)
	mylock = false
	myvelocities = {}
	
	value = -1 -- highest value found so far
	idx = -1   -- index of the highest value
	for i=1,#robot.proximity do
		if value < robot.proximity[i].value then
			idx = i
			value = robot.proximity[i].value
		end
	end
	prox_value = robot.proximity[idx].value
	prox_angle = robot.proximity[idx].angle
	if prox_value > PROX_THRESHOLD and prox_angle > 0 and prox_angle <= math.pi/2 then --closest obstacle on front left
		left_v = prox_value * MAX_VELOCITY
		right_v = 0
		mylock = true
	elseif prox_value > PROX_THRESHOLD and prox_angle < 0 and prox_angle >= -math.pi/2 then --closest obstacle on front right
		left_v = 0
		right_v = prox_value * MAX_VELOCITY
		mylock = true
	end
	
	myvelocities = {left = limit_velocity(left_v), right = limit_velocity(right_v)}
	
	if not lock and mylock then
		robot.leds.set_all_colors("orange")
		log("OA")
		return mylock, myvelocities
	else
		return lock, velocities
	end
end


--[[ This function is executed every time you press the 'execute'
     button ]]
function init()
  	n_steps = -1
	exploring_step = 0
	explored_area_flag = false
	battery_consumed_point_counter = 0
	battery_charging_steps = 0
	explored_area_counter = 0
	already_explorated_area_list = {}
	tmp_already_explorated_area_list = {}

	left_v = robot.random.uniform(0,MAX_VELOCITY)
	right_v = robot.random.uniform(0,MAX_VELOCITY)
	robot.wheels.set_velocity(left_v,right_v)
	robot.gripper.unlock()
end


--[[ This function is executed at each time step
     It must contain the logic of your controller ]]
function step()
	log("Ramaining area: " .. (TOTAL_EXPLORATING_AREAS - explored_area_counter))
	lock, velocities = random_walk(exploring_area(getout_headquarter(get_headquarter(end_work(collision_avoidance(false,{}))))))
	robot.wheels.set_velocity(velocities.left,velocities.right)
end


--[[ This function is executed every time you press the 'reset'
     button in the GUI. It is supposed to restore the state
     of the controller to whatever it was right after init() was
     called. The state of sensors and actuators is reset
     automatically by ARGoS. ]]
function reset()
	n_steps = -1
	exploring_step = 0
	explored_area_flag = false
	battery_consumed_point_counter = 0
	battery_charging_steps = 0
	explored_area_counter = 0
	already_explorated_area_list = {}
	tmp_already_explorated_area_list = {}

	left_v = robot.random.uniform(0,MAX_VELOCITY)
	right_v = robot.random.uniform(0,MAX_VELOCITY)
	robot.wheels.set_velocity(left_v,right_v)
end


--[[ This function is executed only once, when the robot is removed
     from the simulation ]]
function destroy()
end
